# Ruby Script to create a procedural forest map in a Dungeondraft map file
# Version 1.01, Updated: 30-Jan-24, Author: u/uchideshi34

require 'json'
require 'optparse'

$permutation_size = 256

module Bezier
	class ControlPoint
		attr_accessor :x, :y

    # @param x [Numeric]
    # @param y [Numeric]
    #
    # Creates a new control point for the Bézier curve
		def initialize(x,y)
			@x = x
			@y = y
		end

		def - (b)
    	self.class.new(self.x - b.x, self.y - b.y)
    end
    def + (b)
    	self.class.new(self.x + b.x, self.y + b.y)
    end

		# @param point [ControlPoint]
		#
    # @return [ControlPoint] A ControlPoint in a new position
    # Moves a controlpoint with relative distance
    def movepoint (point)
			self.class.new(self.x + point.x, self.y + point.y)
		end

		def inspect
			return @x, @y
		end

		# @return [CurvePoint] Returns a ControlPoint, converted to CurvePoint (this is only a naming difference).
		def to_curvepoint
			CurvePoint.new(self.x, self.y)
		end

    # @return [Array]
    # Returns the control point as an array => [x, y]. The Array is fit to be as argument for ControlPoint.new
    def to_a
			[self.x, self.y]
		end
	end

	class CurvePoint < ControlPoint
		# @return [ControlPoint] point coordinates on the Bézier curve.
		def to_controlpoint
			ControlPoint.new(self.x, self.y)
		end
	end

	class Curve
    # defaults
    DeCasteljau = :decasteljau
    Bernstein = :bernstein

    # this should have been instance variable in the first place, correcting '@@...'
    @calculation_method = Bernstein

		@@fact_memoize = Hash.new
		@@binomial_memoize = Hash.new
		@@pascaltriangle_memoize = Hash.new

		# Ye' olde factorial function
		#
		# @param n [Fixnum]
		# @example
		#   > fact(5)
		def self.fact(n)
			@@fact_memoize[n] ||= (1..n).reduce(:*)
    end

		# @param n [Fixnum]
		# @param k [Fixnum]
		# standard 'n choose k'
		def self.binomial(n,k)
			return 1 if n-k <= 0
			return 1 if k <= 0
			@@binomial_memoize[[n,k]] ||= fact(n) / ( fact(k) * fact( n - k ) )
    end

		# Returns the specified line from the Pascal triangle as an Array
		# @return [Array] A line from the Pascal triangle
		# @example
		#   > pascaltriangle(6)
		def self.pascaltriangle(nth_line) # Classic Pascal triangle
			@@pascaltriangle_memoize[nth_line] ||= (0..nth_line).map { |e| binomial(nth_line, e) }
    end

		# Returns the Bezier curve control points
		#
		# @return [Array<ControlPoints>]

		attr_accessor :controlpoints

		# @param controlpoints [Array<ControlPoints>, Array<(Fixnum, Fixnum)>] list of ControlPoints defining the hull for the Bézier curve. A point can be of class ControlPoint or an Array containig 2 Numerics, which will be converted to ControlPoint.
		# @return [Curve] Creates a new Bézier curve object. The minimum number of control points is currently 3.
		# @example
		#    initialize(p1, p2, p3)
		#    initialize(p1, [20, 30], p3)
		def initialize(*controlpoints)

			# need at least 3 control points
			# this constraint has to be lifted, to allow adding Curves together like a 1 point curve to a 3 point curve
			if controlpoints.size < 3
				raise ArgumentError, 'Cannot create curve with less than 3 control points'
			end

			@controlpoints = controlpoints.map { |point|
				if point.class == Array
					ControlPoint.new(*point[0..1]) # ControlPoint.new gets no more than 2 arguments, excess values are ignored
				elsif point.class == ControlPoint
					point
				else
					raise 'Control points should be type of ControlPoint or Array'
				end
			  }
		end

		# Adds a new control point to the Bezier curve as endpoint.
		#
		# @param [ControlPoint, Array] point
		def add(point)
      @controlpoints << case point
                        when ControlPoint
                          point
                        when Array
                          ControlPoint.new(*point[0..1])
                        else
                          raise(TypeError, 'Point should be type of ControlPoint')
                        end

			# if point.class == ControlPoint
			# 	@controlpoints << point
   #    elsif point.class == Array
   #      @controlpoints << ControlPoint.new(*point[0..1])
			# else
			# 	raise TypeError, 'Point should be type of ControlPoint'
			# end
		end

		# @param [CurvePoint] t
		def point_on_curve(t) # calculates the 'x,y' coordinates of a point on the curve, at the ratio 't' (0 <= t <= 1)
      case
        when @calculation_method == DeCasteljau

        when @calculation_method == Bernstein
          poc_with_bernstein(t)
        else
          poc_with_bernstein(t) # default
      end
    end
    alias_method :poc, :point_on_curve

		def poc_with_bernstein(t)
			n = @controlpoints.size-1
      sum = [0,0]
      for k in 0..n
      	sum[0] += @controlpoints[k].x * self.class.binomial(n,k) * (1-t)**(n-k) * t**k
      	sum[1] += @controlpoints[k].y * self.class.binomial(n,k) * (1-t)**(n-k) * t**k
      end
      return ControlPoint.new(*sum)
      #poc_with_decasteljau(t)
    end

    def point_on_hull(point1, point2, t) # just realized this was nested (geez), Jörg.W.Mittag would have cried. So it is moved out from poc_with_decasteljau()
      if (point1.class != ControlPoint) or (point2.class != ControlPoint)
        raise TypeError, 'Both points should be type of ControlPoint'
      end
      new_x = (1-t) * point1.x + t * point2.x
      new_y = (1-t) * point1.y + t * point2.y
      return ControlPoint.new(new_x, new_y)
    end

		def point_on_curve_binom(t)
			coeffs = self.class.pascaltriangle(self.order)
			coeffs.reduce do |memo, obj|
				memo += t**obj * (1-t)** (n - obj)
			end
		end


		def gnuplot_hull # was recently 'display_points'. just a helper, for quickly put ControlPoints to STDOUT in a gnuplottable format
			@controlpoints.map{|point| [point.x, point.y] }
		end

		def gnuplot_points(precision)
		end

		# returns a new Enumerator that iterates over the Bezier curve from [start_t] to 1 by [delta_t] steps.
		def enumerated(start_t, delta_t)
	  		Enumerator.new do |yielder|
	  			#TODO only do the conversion if start_t is not Float, Fractional or Bigfloat
	  			point_position = start_t.to_f
	    		number = point_on_curve(point_position)
	    		loop do
	      			yielder.yield(number)
	      			point_position += delta_t.to_f
	      			raise StopIteration if point_position > 1.0
	      			number = point_on_curve(point_position)
	    		end
	  		end
		end

		# returns the order of the Bezier curve, aka the number of control points.
		def order
			@controlpoints.size
		end
	end
end

# Create a PoolInt string from an array
def pistr(array)
  str = "PoolIntArray("
  array.each do |value|
    str = "#{str}#{value},"
  end
  str = "#{str.chop} )"
  return str
end

#Create a vector array of points in a circle around a central point
def make_circle(radius, centre, num_points)

	array = Array.new(){Array.new(2)}
	start = Array.new(2)

	start = [centre[0]+radius,centre[1]]
	array << start
	theta = Math::PI * 2 / num_points

	for i in 1..num_points-1 do
		x = Math.cos(theta*i) * radius + centre[0]
		y = Math.sin(theta*i) * radius + centre[1]

		array << [x,y]
	end

	return array

end

# Update the core hash to have the right map size
def update_core_map_hash()

  # Read node_id if it is non-zero
	$next_node_id = 0

  # Update map size
  $base_hash['world']['width'] = $config_hash["core"]["map_size"][0]
  $base_hash['world']['height'] = $config_hash["core"]["map_size"][1]

  $base_hash['header']['editor_state']['camera_position'] = "Vector2( #{$config_hash["core"]["map_size"][0]/2*256}, #{$config_hash["core"]["map_size"][1]/2*256} )"
  $base_hash['header']['editor_state']['camera_zoom'] = 8

  # Update Terrain
  str = " 255, 0, 0, 0," * ($config_hash["core"]["map_size"][0]*$config_hash["core"]["map_size"][1]*16)
  $base_hash['world']['levels']['0']['terrain']['splat'] = "PoolByteArray(#{str.chop})"

	# Update Tiles
	poolintarray = Array.new($config_hash["core"]["map_size"][0]*$config_hash["core"]["map_size"][1],-1)
	$base_hash['world']['levels']['0']['tiles']['cells'] = pistr(poolintarray)

	poolintarray = Array.new($config_hash["core"]["map_size"][0]*$config_hash["core"]["map_size"][1],"ffffffff")
	$base_hash['world']['levels']['0']['tiles']['colors'] = poolintarray

  # Update Cave arrays to have 2xy + 1.5x + 1.5y + 2 (round down) separate entries. No idea why this is.
  str = " 0," * ($config_hash["core"]["map_size"][0]*$config_hash["core"]["map_size"][1]*2 + 1.5 * ($config_hash["core"]["map_size"][0] + $config_hash["core"]["map_size"][1]) + 2).floor()
  $base_hash['world']['levels']['0']['cave']['bitmap'] = "PoolByteArray(#{str.chop})"
  $base_hash['world']['levels']['0']['cave']['entrance_bitmap'] = "PoolByteArray(#{str.chop})"

end

# Shuffle an array
def shuffle(arrayToShuffle)
	for i in 0..arrayToShuffle.length-1
		j = arrayToShuffle.length - i
		index = (rand*(j-1)).round()
		temp = arrayToShuffle[j-1]
		arrayToShuffle[j-1] = arrayToShuffle[index]
		arrayToShuffle[index] = temp
	end
end

#Make Permutation
def makepermutation()
	permutation = Array.new
	for i in 0..$permutation_size-1 do
		permutation.push(i)
	end

	shuffle(permutation)

	for i in 0..$permutation_size-1 do
		permutation.push(permutation[i])
	end

	return permutation
end

def fade(t)
	return ((6*t - 15)*t + 10)*t*t*t
end
def lerp(t, a1, a2)
	return a1 + t*(a2-a1)
end

def grad(hash, x, y)
	h = hash & 15
	u = h < 8 ? x : y
	v = h < 4 ? y : h == 12 || h == 14 ? x : 0
	((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v)
end

def noise2d(x, y)
	cx = x.floor() & ($permutation_size-1)
	cy = y.floor() & ($permutation_size-1)

	xf = x-x.floor()
	yf = y-y.floor()

	u = fade(xf)
	v = fade(yf)

  a = $permutation[cx] + cy
	b = $permutation[cx + 1] + cy

	return lerp(v, lerp(u, grad($permutation[a], xf, yf), grad($permutation[b], xf - 1, yf)),
				 lerp(u, grad($permutation[a + 1], xf, yf - 1), grad($permutation[b + 1], xf - 1, yf - 1)))

end

# Fractal Additional
def fractal(x,y,octaves)
	result = 0.0
	a = 1.0
	f = 0.005
	for i in 0..octaves-1 do
		v = a*noise2d(x*f, y*f)
		result += v
		a *= 0.5
		f *= 2.0
	end
	if result > 1.0 then
		result = 1.0
	elsif result < -1.0 then
		result = -1.0
	end
	return result
end

def createperlin(size,granularity,octaves)
	puts "Making Perlin noise: pixels #{size}, granularity: #{granularity}, octaves: #{octaves}"
	t = Time.now()

	# Create a permutation instance
	$permutation_size = $config_hash["perlin_noise"]["permutation_size"]
	$permutation = makepermutation()


	pixels = Array.new(size){Array.new(size)}
	for y in 0..size-1 do
		if y % (size*0.1).floor() == 0
			puts "Create pixels: #{y} / #{size}"
		end
		for x in 0..size-1 do

			# Noise2D generally returns a value approximately in the range [-1.0, 1.0]
			n = fractal(x, y, octaves)
			# Transform the range to [0.0, 1.0], supposing that the range of Noise2D is [-1.0, 1.0]
			n += 1.0
			n *= 0.5

			pixels[x][y] = (granularity*n).round()
		end
	end
	puts "Perlin noise creation complete. Time taken: #{(Time.now()-t).floor(2)}s.\n\n"
	return pixels
end

def outputppm (filename, array, array_x, array_y,granularity)
	myfile = File.open(filename, "w")
	myfile.write("P2\n")
	myfile.write("# Grayscale version\n\n")
	myfile.write("#{array_x} #{array_y}\n")
	myfile.write("#{granularity}\n")
	for i in 0..array_x-1 do
		for j in 0..array_y-1 do
			myfile.write("#{array[i][j]} ")
		end
		myfile.write("\n")
	end
	myfile.close
end

def outputppmcolourterrain(filename, array, array_x, array_y, granularity)
	rgb = [255,255,255]
	myfile = File.open(filename, "w")
	myfile.write("P3\n")
	myfile.write("#Terrain Colour Map version\n")
	myfile.write("#{array_x} #{array_y}\n")
	myfile.write("255\n")
	for i in 0..array_x-1 do
		for j in 0..array_y-1 do
			value = 255 - (array[i][j] * 255/granularity).to_i()
			if value > $config_hash["terrain"]["high_level"]
				rgb = $config_hash["terrain"]["slots"][0]["colour"]
			elsif value < $config_hash["terrain"]["high_level"] && value > $config_hash["terrain"]["high_level"] - $config_hash["terrain"]["flex_level"]
				rgb = $config_hash["terrain"]["slots"][1]["colour"]
			elsif value < $config_hash["terrain"]["low_level"] && value > 0.0
				rgb = $config_hash["terrain"]["slots"][3]["colour"]
			elsif value < $config_hash["terrain"]["low_level"] && value > $config_hash["terrain"]["low_level"] + $config_hash["terrain"]["flex_level"]
				rgb = $config_hash["terrain"]["slots"][3]["colour"]
			else
				rgb = $config_hash["terrain"]["slots"][2]["colour"]
			end

			myfile.write("#{rgb.join(" ")}\n")
		end
	end
	myfile.close
end

def draw_object(x,y,rotation,size,texture,layer,mirror,custom_color_list)
  object_hash = {"mirror": false}
  object_hash[:position] = "Vector2( #{x*256}, #{y*256} )"
  object_hash[:texture] = texture
  object_hash[:layer] = layer
  object_hash[:scale] = "Vector2( #{size}, #{size} )"
  object_hash[:rotation] = rotation
	object_hash[:mirror] = mirror
  object_hash[:shadow] = false
  if custom_color_list != nil
		object_hash[:custom_color] = custom_color_list.sample
	end
  object_hash[:node_id] = $next_node_id.to_s(16)
  $next_node_id += 1
  $base_hash['world']['levels']['0']['objects'] << object_hash
end

class PoissonDisk
  attr_reader :x_min, :x_max, :y_min, :y_max, :radius, :k, :random, :grid, :point_queue, :first_point

  def initialize(viewport, min_distance, max_tries)
    @x_min = parse(viewport[0], 0)
    @x_max = parse(viewport[1], 0)
    @y_min = parse(viewport[2], 0)
    @y_max = parse(viewport[3], 0)
    @radius = [parse(min_distance, 1), 1].max
    @k = [parse(max_tries, 30), 2].max
    @random = rand
    @grid = {
      width: 0,
      height: 0,
      cell_size: 0,
      data: []
    }
    initialize_parameters
    initialize_state
  end

  def reset
    initialize_state
  end

  def next
    next_point
  end

  def all
    initialize_state
    all_points
  end

  def done
    !first_point && point_queue.empty?
  end

  private

  def parse(x, k)
    Float(x) rescue k
  end

  def initialize_parameters
    grid[:cell_size] = radius * Math.sqrt(1 / 2.0)
    grid[:width] = ((x_max - x_min) / grid[:cell_size]).ceil
    grid[:height] = ((y_max - y_min) / grid[:cell_size]).ceil
    grid[:data] = Array.new(grid[:width] * grid[:height])
  end

  def initialize_state
    @point_queue = []
    @first_point = true
    grid[:data].fill(nil)
  end

  def dist2(x1, y1, x2, y2)
    (x2 - x1)**2 + (y2 - y1)**2
  end

  def create_new_point(x, y)
    point = { x: x, y: y }
    index = (x / grid[:cell_size]).floor + (y / grid[:cell_size]).floor * grid[:width]
    grid[:data][index] = point
    point_queue.push(point)
    point
  end

  def is_valid_point(x, y)
    return false if x < x_min || x > x_max || y < y_min || y > y_max

    col = ((x - x_min) / grid[:cell_size]).floor
    row = ((y - y_min) / grid[:cell_size]).floor

    for i in (col - 2)..(col + 2)
      for j in (row - 2)..(row + 2)
        if i >= 0 && i < grid[:width] && j >= 0 && j < grid[:height]
          idx = i + (j * grid[:width])
          if grid[:data][idx] && dist2(x, y, grid[:data][idx][:x], grid[:data][idx][:y]) <= (radius**2)
            return false
          end
        end
      end
    end

    true
  end

  def next_point
    x, y = 0, 0

    if first_point
      @first_point = false
      x = x_min + (x_max - x_min) * rand()
      y = y_min + (y_max - y_min) * rand()
      return create_new_point(x, y)
    end

    idx, distance, angle = 0, 0, 0

    while point_queue.any?
      idx = (point_queue.length * rand()).to_i
      for i in 0..k-1 do
        distance = radius * (rand() + 1)
        angle = 2 * Math::PI * rand()
        x = point_queue[idx][:x] + distance * Math.cos(angle)
        y = point_queue[idx][:y] + distance * Math.sin(angle)
        return create_new_point(x, y) if is_valid_point(x, y)
      end
      point_queue.delete_at(idx)
    end

    nil
  end

  def all_points
    result = []

    loop do
      point = next_point()
      break if point.nil?

      result.push(point)
    end

    result
  end
end


def store_perlin(array, outputfile)

  hash = Hash.new
  hash[:perlin] = array
  File.open(outputfile, "wb") { |file| file.puts JSON.pretty_generate(hash) }

end

def retrieve_perlin(inputfile)
  hash = JSON.parse(File.read(inputfile))
  return hash["perlin"]
end

# Returns a 3d array of the terrain values
def make_terrain_array(splat_str, splat2_str,max_x_squares,max_y_squares)
	splat = Array.new
	splat2 = Array.new
	t = Time.now

	puts "Creating terrain array..."
	splat_str = splat_str.gsub('PoolByteArray( ','')
	splat_str = splat_str.chop()
	splat = splat_str.split(", ")

	if splat2_str.include? 'PoolByteArray'
		splat2_str = splat2_str.gsub('PoolByteArray( ','')
		splat2_str = splat2_str.chop()
		splat2 = splat2_str.split(", ")
		xtraterrain = true
	end
	if xtraterrain
		output = Array.new(max_x_squares*4){Array.new(max_y_squares*4){Array.new(8)}}
	else
		output = Array.new(max_x_squares*4){Array.new(max_y_squares*4){Array.new(4)}}
	end
	#puts "splat #{splat} #{splat.length()}"
	for i in 0..max_x_squares*4-1 do
		for j in 0..max_y_squares*4-1 do
			index=j*max_x_squares*4

			output[i][j][0] = splat[i*4+index]
			output[i][j][1] = splat[i*4+index+1]
			output[i][j][2] = splat[i*4+index+2]
			output[i][j][3] = splat[i*4+index+3]
			if xtraterrain
				output[i][j][4] = splat2[i*4+index]
				output[i][j][5] = splat2[i*4+index+1]
				output[i][j][6] = splat2[i*4+index+2]
				output[i][j][7] = splat2[i*4+index+3]
			end

		end
	end
	puts "Terrain array creation complete... Time taken #{(Time.now-t).floor(3)}s.\n\n"
	return output
end

def apply_perlin_to_terrain(perlin, terrain, granularity, size, max_x_squares, max_y_squares)
	puts "Applying Perlin to terrain..."

	high = $config_hash["terrain"]["high_level"]
	low = $config_hash["terrain"]["low_level"]
	flex = $config_hash["terrain"]["flex_level"]
	open_area_weight = $config_hash["terrain"]["open_area_weight"]
	highest_weight = $config_hash["terrain"]["highest_weight"]
	low_weight = $config_hash["terrain"]["low_weight"]
	high_flex_factor = $config_hash["terrain"]["high_flex_factor"]
	low_flex_factor = $config_hash["terrain"]["low_flex_factor"]
	t = Time.now

	for i in 0..max_x_squares*4-1 do
		for j in 0..max_y_squares*4-1 do
			#Find nearest Perlin point
			nearest_x = (i*size/(max_x_squares*4)).to_i()
			nearest_y = (j*size/(max_x_squares*4)).to_i()
			value = 255 - (perlin[nearest_x][nearest_y] * 255/granularity).to_i()
			terrain[i][j][2] = (255-value).to_i()
			terrain[i][j][0] = 0
			terrain[i][j][1] = 0
			terrain[i][j][3] = 0
			case value
			when high-flex..high
				terrain[i][j][0] = (value*highest_weight*high_flex_factor).to_i()
				terrain[i][j][1] = 255 - terrain[i][j][2] - terrain[i][j][0]
				terrain[i][j][3] = 0
			when high..255
				terrain[i][j][0] = (value*highest_weight).to_i()
				terrain[i][j][1] = 255 - terrain[i][j][2] - terrain[i][j][0]
				terrain[i][j][3] = 0
			when 0..low
				terrain[i][j][3] = (255 - value)*low_weight*low_flex_factor
				terrain[i][j][2] = 255 - terrain[i][j][3]
			when low..low+flex
				terrain[i][j][3] = (255 - value)*low_weight
				terrain[i][j][2] = 255 - terrain[i][j][3]
			else
				terrain[i][j][2] = [((255-value)*open_area_weight),255].min().to_i()
				terrain[i][j][1] = 255 - terrain[i][j][2]
			end

		end
	end
	puts "Perlin successfully applied to terrain. Time taken #{(Time.now-t).floor(3)}s.\n\n"

end

def output_terrain(output,xtraterrain,max_x_squares,max_y_squares)
	splat = Array.new
	splat2 = Array.new
	t = Time.now

	puts "Writing out terrain to hash..."
	if !xtraterrain
		$base_hash['world']['levels']['0']['terrain']['splat'] = "PoolByteArray(#{output.flatten().join(", ")})"
		puts "Finished applying terrain to hash. Time taken #{(Time.now-t).floor(3)}s"
		return
	end
	for i in 0..max_x_squares*4-1 do
		for j in 0..max_y_squares*4-1 do
			splat += output[i][j][0..3]
			splat2 += output[i][j][4..7]
		end
	end
	puts "Made terrain arrays for splat and splat2. Time taken #{(Time.now-t).floor(3)}s."

	for i in 0..3 do
		if $config_hash["terrain"]["slots"][i]["overwrite_slot"]
			$base_hash['world']['levels']['0']['terrain']["texture_#{i+1}"]=$config_hash["terrain"]["slots"][i]["texture"]
		end
	end

	$base_hash['world']['levels']['0']['terrain']['splat'] = "PoolByteArray(#{splat.flatten().join(", ")})"
	$base_hash['world']['levels']['0']['terrain']['splat2'] = "PoolByteArray(#{splat2.flatten().join(", ")})"
	puts "Finished applying terrain to hash."
	return
end

def create_rotation(randomrotation,rotation_offset)

	if rotation_offset == nil
		rotation_offset = 0
	end

	if randomrotation
		rotation = rand*2.0*Math::PI - Math::PI
	elsif (rotation_offset.to_i()).abs() > 0
		rotation = ((rotation_offset+180) % 360 - 180) / 360.0 * 2.0 * Math::PI
	else
		rotation = 0.0
	end
	return rotation
end

def create_mirror(randommirror)
	if randommirror
		mirror = [true, false].sample
	else
		mirror = false
	end
	return mirror
end

#Draw a clump at point x,y
def draw_clump(x,y,clump_hash,index)
	rotation = 0.0
	mirror = false

	num_assets = rand(clump_hash["primary_asset_list"][index]["num_secondary_assets_range"][0]..clump_hash["primary_asset_list"][index]["num_secondary_assets_range"][1])

	#Draw the secondary assets
	for i in 0..num_assets-1 do
		direction = rand(i.to_f()/num_assets..(i+1).to_f()/num_assets) * 2 * Math::PI - Math::PI

		distance = rand(2*clump_hash["primary_asset_list"][index]["secondary_assets_dist_variation_px"]) + clump_hash["primary_asset_list"][index]["secondary_assets_dist_px"] - clump_hash["primary_asset_list"][index]["secondary_assets_dist_variation_px"]
		distance = distance / 256.0

		out_x = Math.cos(direction) * distance + x
		out_y = Math.sin(direction) * distance + y

		texture = clump_hash["secondary_asset_list"].sample

		rotation = create_rotation(clump_hash["secondary_randomrotation"],0.0)
		mirror = create_mirror(clump_hash["primary_asset_list"][index]["secondary_randommirror"])

		draw_object(out_x,out_y,rotation,1.0,texture,clump_hash["secondary_asset_layer"],mirror,clump_hash["secondary_asset_colour_list"])

	end

	#Draw the primary asset
	rotation = create_rotation(clump_hash["primary_asset_list"][index]["randomrotation"],clump_hash["primary_asset_list"][index]["rotation_offset"])
	mirror = create_mirror(clump_hash["primary_asset_list"][index]["randommirror"])

	draw_object(x,y,rotation,1.0,clump_hash["primary_asset_list"][index]["texture"],clump_hash["layer"],mirror,clump_hash["primary_asset_colour_list"])

end

# Determine a location for a clump and then draw it there
def scatter_clump(pixels,clump_hash)

	viewport = [0,$config_hash["core"]["map_size"][0],0,$config_hash["core"]["map_size"][1]]
	poissonDisk = PoissonDisk.new(viewport, clump_hash["mindistance"], $config_hash["poissondisk"]["max_tries"])

	points = poissonDisk.all()
	list = Array.new

	#Make a list of indices to the primary_asset_list
	for i in 0..clump_hash["primary_asset_list"].length()-1
		list[i] = i
	end

	#Copy that list
	primary_list = list.map(&:clone)

	for i in 0..points.length-1 do
			pixelmatch_x = ((points[i][:x]*$config_hash["perlin_noise"]["size"]/$config_hash["core"]["map_size"][0]).floor())
			pixelmatch_y = ((points[i][:y]*$config_hash["perlin_noise"]["size"]/$config_hash["core"]["map_size"][1]).floor())

			if pixels[pixelmatch_x][pixelmatch_y] < clump_hash["upper_perlin_level"]*$config_hash["perlin_noise"]["granularity"] && pixels[pixelmatch_x][pixelmatch_y] > clump_hash["lower_perlin_level"]*$config_hash["perlin_noise"]["granularity"] && rand < clump_hash["probability"] then

				if primary_list.length()<2 then
					primary_list = list.map(&:clone)
				end
				index = rand(primary_list.length())

				draw_clump(points[i][:y],points[i][:x],clump_hash,primary_list[index])

				primary_list.delete_at(index)

			end
	end

end

def scatter_clumps(pixels)

	if !$config_hash.has_key?("clumps")
		return
	end

	puts "Drawing clumps of objects."
	t = Time.now()

	for i in 0..$config_hash["clumps"].length()-1 do
		scatter_clump(pixels,$config_hash["clumps"][i])
	end

	puts "Finished drawing clumps. Time taken: #{(Time.now()-t).floor(2)}s.\n\n"

end

def scatter_object_layer(pixels,mindistance,probability,upper_perlin_level,lower_perlin_level,randomrotation,size,layer,list,randommirror,rotation_offset,custom_color_list)

	viewport = [0,$config_hash["core"]["map_size"][0],0,$config_hash["core"]["map_size"][1]]
	poissonDisk = PoissonDisk.new(viewport, mindistance, $config_hash["poissondisk"]["max_tries"])

	points = poissonDisk.all()
	object_list = Array.new
	object_list = list.map(&:clone)


	for i in 0..points.length-1 do
			pixelmatch_x = ((points[i][:x]*$config_hash["perlin_noise"]["size"]/$config_hash["core"]["map_size"][0]).floor())
			pixelmatch_y = ((points[i][:y]*$config_hash["perlin_noise"]["size"]/$config_hash["core"]["map_size"][1]).floor())

			if pixels[pixelmatch_x][pixelmatch_y] < upper_perlin_level*$config_hash["perlin_noise"]["granularity"] && pixels[pixelmatch_x][pixelmatch_y] > lower_perlin_level*$config_hash["perlin_noise"]["granularity"] && rand < probability then
				rotation = create_rotation(randomrotation,rotation_offset)
				mirror = create_mirror(randommirror)
				if object_list.length()<2 then
					object_list = list.map(&:clone)
				end
				index = rand(object_list.length())
				draw_object(points[i][:y],points[i][:x],rotation,rand*(size[1]-size[0])+size[0],object_list[index],layer,mirror,custom_color_list)

				object_list.delete_at(index)

			end
	end

end

def scatter_trees(pixels)

	puts "Scattering trees..."
	tree_type = Hash.new

	for i in 0..$config_hash["trees"]["tree_type_list"].length()-1 do
		tree_type = $config_hash["trees"]["tree_type_list"][i]
		scatter_object_layer(pixels,tree_type["mindistance"],tree_type["probability"],tree_type["upper_perlin_level"],tree_type["lower_perlin_level"],tree_type["randomrotation"],tree_type["size"],tree_type["layer"],tree_type["list"],false,tree_type["rotation_offset"],tree_type["custom_colour_list"])

	end
	puts "Scatter trees complete."

end

def draw_light(x,y,light_colour,light_intensity,light_radius)

	light_hash = Hash.new

	light_hash = {
		"position": "Vector2( 3444, 2976 )",
		"rotation": 0,
		"texture": "res://textures/lights/soft.png",
		"shadows": true
	}

	light_hash[:node_id] = $next_node_id.to_s(16)
	$next_node_id += 1
	light_hash[:range] = light_radius
	light_hash[:intensity] = light_intensity
	light_hash[:color] = light_colour
	light_hash[:position] = "Vector2( #{x*256}, #{y*256} )"

	$base_hash['world']['levels']['0']['lights'] << light_hash

end

def draw_shadow_object_for_tree(obj_index, tree_index)

	obj_location = $base_hash['world']['levels']['0']['objects'][obj_index][:position][9..$base_hash['world']['levels']['0']['objects'][obj_index][:position].length()-2].split(", ")
	x = obj_location[0].to_f()/256.0
	y = obj_location[1].to_f()/256.0

	rotation = ($config_hash["trees"]["tree_type_list"][tree_index]["rotation_offset"] + $config_hash["trees"]["tree_type_list"][tree_index]["shadows"]["rotation_offset"]).floor() % 360

	size = $config_hash["trees"]["tree_type_list"][tree_index]["shadows"]["size"]
	texture = $config_hash["trees"]["tree_type_list"][tree_index]["shadows"]["texture_list"].sample
	layer = $config_hash["trees"]["tree_type_list"][tree_index]["layer"]
	mirror = create_mirror($config_hash["trees"]["tree_type_list"][tree_index]["shadows"]["randommirror"])

 	if $config_hash["trees"]["tree_type_list"][tree_index]["shadows"].has_key?("centre_offset_dist")
 		distance = $config_hash["trees"]["tree_type_list"][tree_index]["shadows"]["centre_offset_dist"]
		angle = ((rotation + $config_hash["trees"]["tree_type_list"][tree_index]["shadows"]["centre_offset_angle"].floor()) % 360) / 360.0 * 2 * Math::PI - Math::PI
		x += - Math.sin(angle) * distance
		y += Math.cos(angle) * distance
	end

	rotation = rotation / 360.0 * 2 * Math::PI - Math::PI

	draw_object(x,y,rotation,size,texture,layer,mirror,["ff000000"])

	if $config_hash["trees"]["tree_type_list"][tree_index]["shadows"]["place_under"]
		$base_hash['world']['levels']['0']['objects'][-1][:node_id] = $base_hash['world']['levels']['0']['objects'][obj_index][:node_id]
		$base_hash['world']['levels']['0']['objects'][obj_index][:node_id] = $next_node_id
		$base_hash['world']['levels']['0']['objects'][-1], $base_hash['world']['levels']['0']['objects'][obj_index] = $base_hash['world']['levels']['0']['objects'][obj_index], $base_hash['world']['levels']['0']['objects'][-1]
	end


end



def apply_shadows_to_trees()
	obj_location = Array.new(2)
	obj_index = Array.new(2)
	object_array = Array.new(){Array.new(2)}
	store_delete_index = Array.new()
	t = Time.now()
	puts "Apply shadows to trees..."

	for i in 0..$base_hash['world']['levels']['0']['objects'].length()-1 do

		obj_location = $base_hash['world']['levels']['0']['objects'][i][:position][9..$base_hash['world']['levels']['0']['objects'][i][:position].length()-2].split(", ")
		texture = $base_hash['world']['levels']['0']['objects'][i][:texture]


		for j in 0..$config_hash["trees"]["tree_type_list"].length()-1 do
			if $config_hash["trees"]["tree_type_list"][j].has_key?("shadows")
				if $config_hash["trees"]["tree_type_list"][j]["list"].include?(texture)
					if $config_hash["trees"]["tree_type_list"][j]["shadows"]["make_shadow"]
						if $config_hash["trees"]["tree_type_list"][j]["shadows"]["type"] == "path"

							shadow_array = make_circle($config_hash["trees"]["tree_type_list"][j]["shadows"]["asset_radius"]/256.0, [obj_location[1].to_f()/256.0,obj_location[0].to_f()/256.0], 25)
							draw_path(shadow_array, 1, $config_hash["trees"]["tree_type_list"][j]["shadows"]["size"], $config_hash["trees"]["tree_type_list"][j]["shadows"]["texture"], $config_hash["trees"]["tree_type_list"][j]["layer"],true)

						end
						if $config_hash["trees"]["tree_type_list"][j]["shadows"]["type"] == "object"

							draw_shadow_object_for_tree(i,j)

						end

					end

				end
			end
			if $config_hash["trees"]["tree_type_list"][j].has_key?("lights")
				if $config_hash["trees"]["tree_type_list"][j]["list"].include?(texture)
					if $config_hash["trees"]["tree_type_list"][j]["lights"]["draw_lights"] && $config_hash["lighting"]["draw_lights"]
						draw_light(obj_location[0].to_f()/256.0,obj_location[1].to_f()/256.0,$config_hash["trees"]["tree_type_list"][j]["lights"]["light_colour"],$config_hash["trees"]["tree_type_list"][j]["lights"]["light_intensity"],$config_hash["trees"]["tree_type_list"][j]["lights"]["light_radius"])
					end
				end
			end
		end
	end

	puts "Completed applying shadows to trees. Time taken: #{(Time.now()-t).floor(2)}s.\n\n"
end

def scatter_objects(pixels)
	puts "Scattering ground clutter..."

	for i in 0..$config_hash["clutter"].length()-1 do
		object_type = $config_hash["clutter"][i]
		scatter_object_layer(pixels,object_type["mindistance"],object_type["probability"],object_type["upper_perlin_level"],object_type["lower_perlin_level"],object_type["randomrotation"],object_type["size"],object_type["layer"],object_type["list"],object_type["randommirror"],0,object_type["custom_colour_list"])
	end
	puts "Scatter ground clutter complete.\n\n"

end

# Compress perlin down to a 2d array with 16 points per map square
def simplifyperlin(perlin)
	puts "Simplifying Perlin map down to array with 16 points per square."
	t = Time.now

	output = Array.new($config_hash["core"]["map_size"][0]*4){Array.new($config_hash["core"]["map_size"][1]*4)}

	for i in 0..$config_hash["core"]["map_size"][0]*4-1 do
		for j in 0..$config_hash["core"]["map_size"][1]*4-1 do
			nearest_x = (i*$config_hash["perlin_noise"]["size"]/($config_hash["core"]["map_size"][0]*4)).floor()
			nearest_y = (j*$config_hash["perlin_noise"]["size"]/($config_hash["core"]["map_size"][1]*4)).floor()
			output[i][j] = perlin[nearest_x][nearest_y]
		end
	end

	puts "Finished simplying Perlin. Time taken: #{(Time.now-t).floor(3)}s.\n\n"
	return output

end

def outputppmcolourwalk(filename, array, array_x, array_y, granularity, walkpath)
	myfile = File.open(filename, "w")
	myfile.write("P3\n")
	myfile.write("# Colour walk\n")
	myfile.write("#{array_x} #{array_y}\n")
	myfile.write("255\n")
	for i in 0..array_x-1 do
		for j in 0..array_y-1 do
			found = 0
			for k in 0..walkpath.length()-1 do
				if walkpath[k] == [i,j]
					myfile.write("255 0 0\n")
					found = 1
					break
				end
			end
			if found == 0
				output = array[i][j]*255/granularity.floor()
				myfile.write("#{output} #{output} #{output}\n")
			end
		end
	end
	myfile.close
end

def getsteplocations(maparray,start,direction,fov_angle,step_distance,stepoptions)

		stepoptions.clear()

		num_options = (fov_angle * 2.0 * step_distance * 4.5).ceil()
		#puts "num_options: #{num_options}, fov_angle #{fov_angle}, direction #{direction*360.0/(2*Math::PI)}, step_distance #{step_distance}"

		for i in 0..num_options-1 do
			theta = (2.0*fov_angle/num_options*i - fov_angle + direction)
			x = -(Math.sin(theta) * step_distance*4).round()
			y = (Math.cos(theta) * step_distance*4).round()
			#puts "theta: #{(theta*360.0/(2*Math::PI)).floor(1)}, direction #{(direction*360.0/(2*Math::PI)).floor(1)}, Step Options: x: #{x.floor(1)}, y: #{y.floor(1)}, step_distance #{step_distance}"

      if start[0]+x < 0 || start[0]+x > $config_hash["core"]["map_size"][0]*4-1 || start[1]+y < 0 || start[1]+y > $config_hash["core"]["map_size"][1]*4-1
				next
			end
			stepoptions << [start[0]+x,start[1]+y]
			#puts "Step Options: x: #{x}, y: #{y}"
		end
		return stepoptions.uniq()
end

def findbeststepoption(maparray,stepoptions)

	best_value = 0
	best_index = [0,0]
	weight = $config_hash["road"]["progress_weight"]
	margin = ($config_hash["road"]["margin_percentage"]*$config_hash["core"]["map_size"][1]*2).floor()

	for i in 0..stepoptions.length()-1 do

    test_value = maparray[stepoptions[i][0]][stepoptions[i][1]]+stepoptions[i][1]*weight
    if stepoptions[i][0] < margin+$config_hash["road"]["width"]*2 || stepoptions[i][0] > $config_hash["core"]["map_size"][1]*4-margin-$config_hash["road"]["width"]*2
			test_value = test_value * 0.25

		end
    #puts "location: x: #{stepoptions[i][0]}, y: #{stepoptions[i][1]}, maparray: #{maparray[stepoptions[i][0]][stepoptions[i][1]]}"
		if best_value < test_value
			#puts "Found better: old value: #{best_value}, new value: #{test_value}, location: x: #{stepoptions[i][0]}, y: #{stepoptions[i][1]}"
			best_value = test_value
			best_index = [stepoptions[i][0],stepoptions[i][1]]
		end
	end

	return best_index

end


def transposearrayofcoords(twodarray)

	new_array = twodarray.map(&:clone)

	for i in 0..twodarray.length()-1 do
		new_array[i][1] = twodarray[i][0]
		new_array[i][0] = twodarray[i][1]
	end
	return new_array
end

#Note that x and y are flipped for this
def draw_path(vector_array, end_point_type, width, texture, layer, loop)
  path = {
    "rotation": 0,
    "scale": "Vector2( 1, 1 )",
    "smoothness": 1,
    "layer": layer,
    "fade_in": false,
    "fade_out": false,
    "grow": false,
    "shrink": false,
    "loop": false,
  }
	#puts "vector_array #{vector_array}"
  path[:texture] = texture
  path[:position] = "Vector2( #{vector_array[0][1]*256}, #{vector_array[0][0]*256} )"
  path[:width] = width
  path[:edit_points] = "PoolVector2Array( 0, 0"
  for i in 1..vector_array.length-1 do
    path[:edit_points] += ", #{(vector_array[i][1]-vector_array[0][1])*256}, #{(vector_array[i][0]-vector_array[0][0])*256}"
  end
  path[:edit_points] += " )"
  path[:node_id] = $next_node_id.to_s(16)
  if end_point_type == 0
    path[:fade_in] = true
    path[:fade_out] = true
	elsif end_point_type == 2
		path[:grow] = true
		path[:shrink] = true
  end
	path[:loop] = loop
  $base_hash['world']['levels']['0']['paths'] << path
  $next_node_id += 1
end

def findwalkstart(simple_map)

	highest = 0
	index = 0
	margin = ($config_hash["road"]["margin_percentage"]*$config_hash["core"]["map_size"][1]*4).floor()+$config_hash["road"]["width"]*4

	for i in margin..$config_hash["core"]["map_size"][1]*4-margin-1 do
		if highest < simple_map[0][i]
			highest = simple_map[0][i]
			index = i
		end
		#puts "highest: #{highest} simple_map: #{simple_map[0][i]}, i: 0, j: #{i}"
	end

	return [index,0]
end

def draw_pattern(polygonarray, layer, texture, colour_str, rotation)

	pattern_hash = Hash.new()
	vector_array = Array.new()

	pattern_hash = {
		"position": "Vector2( 0, 0 )",
		"shape_rotation": 0,
		"scale": "Vector2( 1, 1 )",
		"points": "PoolVector2Array( 384, 2048, 2176, 2048, 2176, 2944, 384, 2944 )",
		"outline": false
	}

	pattern_hash[:rotation] = (rotation / 360.0) * 2 * Math::PI
	pattern_hash[:color] = colour_str
	pattern_hash[:texture] = texture
	pattern_hash[:node_id] = $next_node_id.to_s(16)
	$next_node_id += 1
	pattern_hash[:layer] = layer

	for i in 0..polygonarray.length()-1 do
		vector_array << polygonarray[i]*256
	end

	pattern_hash[:points] = "PoolVector2Array( #{vector_array.join(", ")} )"

	$base_hash['world']['levels']['0']['patterns'] << pattern_hash

	return
end

def outputwater(waterpolygonarray)

	vector_array = Array.new()
	water_hash = Hash.new()

	water_hash = {
		"disable_border": true,
		"tree": {
			"ref": 951149238,
			"polygon": "PoolVector2Array(  )",
			"join": 0,
			"end": 0,
			"is_open": false,
			"deep_color": "00000000",
			"shallow_color": "00000000",
			"blend_distance": 0,
			"children": []
			}
		}

	water_instance_hash = {
		"ref": -1663965590,
		"polygon": "PoolVector2Array( 768, 512, 1280, 512, 1280, 1024, 1536, 1024, 1536, 1536, 1024, 1536, 1024, 1280, 512, 1280, 512, 768, 256, 768, 256, 256, 768, 256 )",
		"join": 0,
		"end": 0,
		"is_open": false,
		"deep_color": "ff3aa19a",
		"shallow_color": "ff8bceb0",
		"blend_distance": 1.5,
		"children": []

	}

	water_instance_hash[:blend_distance] = $config_hash["road"]["river"]["blend_distance"]
	water_instance_hash[:deep_color] = $config_hash["road"]["river"]["deep_color"]
	water_instance_hash[:shallow_color] = $config_hash["road"]["river"]["shallow_color"]

	for i in 0..waterpolygonarray.length()-1 do
		vector_array << waterpolygonarray[i]*256
	end

	water_instance_hash[:polygon] = "PoolVector2Array( #{vector_array.join(", ")} )"

	water_hash[:tree][:children] << water_instance_hash

	$base_hash['world']['levels']['0']['water'] = water_hash

end


def goforawalk(maparray,start,end_type,fov_angle,step_distance)

	walkpath = Array.new(){Array.new(2)}
	stepoptions = Array.new(){Array.new(2)}

	walkpath << start.map(&:clone)

	direction = 0.0

	current_location = start.map(&:clone)
	i = 0

	while i < 500 do

		if current_location[1] + step_distance*4 > $config_hash["core"]["map_size"][1]*4
			#puts "Stopping walk: #{current_location[1] + step_distance*4} map size: #{$config_hash["core"]["map_size"][1]*4}"
			walkpath << [current_location[0],current_location[1]+step_distance*4]
			break
		end
    # Go straight out for the first step
		if i == 0
      stepoptions = [[current_location[0],current_location[1]+step_distance*4.0]]
		else
			stepoptions = getsteplocations(maparray,current_location,direction,fov_angle,step_distance,stepoptions)
		end

		if stepoptions.length() == 0
			break
		end
		best = findbeststepoption(maparray,stepoptions)
		#puts "best: #{best[0]*0.25}, #{best[1]*0.25}"

		walkpath << best

		direction = - Math.atan2(best[0]-current_location[0],best[1]-current_location[1])
		current_location = best
		i += 1
	end

	return transposearrayofcoords(walkpath)
end

# Function assumes incoming array has 16 areas per map square and outputs for a standard map square definition
def makebeziercurve(array,delta)

  output = Array.new(){Array.new(2)}
  ctrpnts_array = Array.new()

  #output << [array[0][0],array[0][1]]

  #num_points in each arc
  num_points = (($config_hash["core"]["map_size"][0]*4*1.0/delta)/array.length()).to_i()
  #puts "num_points #{num_points}, array.length() #{array.length()}"

  #Draw bezier curves to midpoints and in triples
  for i in 0..array.length()-3 do
    if i == 0
      curve_start = [(array[1][0]-array[0][0])*0.5+array[0][0],(array[1][1]-array[0][1])*0.5+array[0][1]]
      curve_middle = [array[1][0],array[1][1]]
      # find mid point and define that as curve end
      curve_end = [(array[2][0]-array[1][0])*0.5+array[1][0],(array[2][1]-array[1][1])*0.5+array[1][1]]
      # Fill in the initial first few points
      for j in 0..(num_points/2).to_i() do

				temp = [((curve_start[0]-array[0][0])*0.5*j*2/(num_points/2)+array[0][0]),((curve_start[1]-array[0][1])*0.5*2/(num_points/2)+array[0][1])]
				#puts "array: [#{(array[0][0]/2.0).round(2)}, #{(array[0][1]/2.0).round(2)}] curve_start [#{(curve_start[0]/2.0).round(2)}, #{(curve_start[1]/2.0).round(2)}], initial walk: [#{(temp[0]/2.0).round(2)}, #{(temp[1]/2.0).round(2)}]"
				output << temp

      end
    elsif i == array.length()-3
      curve_start = curve_end
      curve_middle = [array[i+1][0],array[i+1][1]]
      curve_end = [array[i+2][0],array[i+2][1]]
    else
      curve_start = curve_end
      curve_middle = [array[i+1][0],array[i+1][1]]
      curve_end = [(array[i+2][0]-array[i+1][0])*0.5+array[i+1][0],(array[i+2][1]-array[i+1][1])*0.5+array[i+1][1]]
    end

    bez = Bezier::Curve.new(curve_start, curve_middle, curve_end)
    for j in 0..num_points-1 do
      output << [bez.point_on_curve(j.to_f()*1/num_points).x,bez.point_on_curve(j.to_f()*1/num_points).y]
    end

  end

  output << [curve_end[0],curve_end[1]]
  return output
end

def calculate_perpendicular_position(x, y, direction, distance)

  # Calculate new x and y coordinates
  new_x = x + distance * Math.cos(direction + Math::PI/2)
  new_y = y + distance * Math.sin(direction + Math::PI/2)

  return [new_x, new_y]
end

def findroadarea(lefthandside,righthandside,terrain)
	# define a road area as a series of point pairs by tracing the line between equivalent left and right paths
	road_area = Array.new($config_hash["core"]["map_size"][0]*4){Array.new($config_hash["core"]["map_size"][1]*4,0.0)}
	delta = Array.new(2)

	incremental_steps = 10
	steps = ($config_hash["road"]["width"] * incremental_steps).floor()
	edge = ($config_hash["road"]["edge"]["roadedge_width"] * incremental_steps).floor()
	step_distance = 1.0/steps
	max_x_index = $config_hash["core"]["map_size"][0]*4 - 1
	max_y_index = $config_hash["core"]["map_size"][1]*4 - 1
	previous_x = -1
	previous_y = -1
	previous_start_left_x = -1
	previous_start_left_y = -1
	previous_start_right_x = -1
	previous_start_right_y = -1

	for i in 0..lefthandside.length()-1 do
		start_left_x = (lefthandside[i][0]*4.0).floor()
		start_left_y = (lefthandside[i][1]*4.0).floor()
		start_right_x = (righthandside[i][0]*4.0).floor()
		start_right_y = (righthandside[i][1]*4.0).floor()

		if start_left_x == previous_start_left_x && start_left_y == previous_start_left_y && start_right_x == previous_start_right_x && start_right_y == previous_start_right_y
			next
		else
			previous_start_left_x = start_left_x
			previous_start_left_y = start_left_y
			previous_start_right_x = start_right_x
			previous_start_right_y = start_right_y
		end

		delta[0] = (righthandside[i][0] - lefthandside[i][0])/steps
		delta[1] = (righthandside[i][1] - lefthandside[i][1])/steps

		for j in 0..steps do
			x = ((lefthandside[i][0] + j * delta[0])*4.0).floor()
			y = ((lefthandside[i][1] + j * delta[1])*4.0).floor()

			if previous_x == x && previous_y == y
				next
			else
				previous_x = x
				previous_y = y
			end

			if x < 0 || x > max_x_index || y < 0 || y > max_y_index || road_area[x][y] > 0
				next
			end

			#If we are within a distance from the edge then record that
			if j <= edge || j >= steps - edge
				if j <= edge
					dist_from_edge = j.to_f() / edge
				else
					dist_from_edge = (steps - j).to_f() / edge
				end
				road_area[x][y] = dist_from_edge
			else
				road_area[x][y] = 2
				next
			end

		end
		previous_x = -1
		previous_y = -1
	end

	return road_area

end


def linear_interpolation(a,b,distance)

	return ((b - a) * distance + a).floor()

end

def apply_terrain_to_road(simple_perlin_map,road_area,terrain)

	target_terrain = Array.new(4)

	for x in 0..$config_hash["core"]["map_size"][0]*4-1 do
		for y in 0..$config_hash["core"]["map_size"][1]*4-1 do
			if road_area[x][y] > 0.0

				target_terrain[0] = 255
				target_terrain[1] = 0
				target_terrain[2] = 0
				target_terrain[3] = 0


				#If we are going to vary the terrain of the road with terrain_3 then
				if $config_hash["road"].has_key?("terrain")
					if $config_hash["road"]["terrain"]["vary_road_terrain"]

						perlin_value = simple_perlin_map[x][y].to_f()/$config_hash["perlin_noise"]["granularity"]
						target_terrain[0] = ((perlin_value * ($config_hash["road"]["terrain"]["terrain_1_highest_value"]-$config_hash["road"]["terrain"]["terrain_1_lowest_value"]))+$config_hash["road"]["terrain"]["terrain_1_lowest_value"]).floor()
						target_terrain[3] = 255 - target_terrain[0]

						if road_area[x][y] < 2.0
							target_terrain[0] = linear_interpolation($config_hash["road"]["terrain"]["edge_terrain_1_value_255"],target_terrain[0],road_area[x][y])
							target_terrain[3] = 255 - target_terrain[0]
						end
					end
				end


				if road_area[x][y] < 2.0
					dist_from_edge = road_area[x][y]
					target_terrain[0] = linear_interpolation(terrain[y][x][0],target_terrain[0],dist_from_edge)
					target_terrain[1] = linear_interpolation(terrain[y][x][1],target_terrain[1],dist_from_edge)
					target_terrain[2] = linear_interpolation(terrain[y][x][2],target_terrain[2],dist_from_edge)
					target_terrain[3] = 255 - target_terrain[0] - target_terrain[1] - target_terrain[2]
				end

				terrain[y][x][0] = target_terrain[0]
				terrain[y][x][1] = target_terrain[1]
				terrain[y][x][2] = target_terrain[2]
				terrain[y][x][3] = target_terrain[3]
			end
		end
	end

end


def removeobjectsfromroad(road_area)

	obj_location = Array.new(2)
	obj_index = Array.new(2)
	size_factor = 1.0
	object_array = Array.new(){Array.new(2)}
	store_delete_index = Array.new()

	for i in 0..$base_hash['world']['levels']['0']['objects'].length()-1 do

		if $base_hash['world']['levels']['0']['objects'][i] == nil
			puts "Index is wrong"
			next
		end

		obj_location = $base_hash['world']['levels']['0']['objects'][i][:position][9..$base_hash['world']['levels']['0']['objects'][i][:position].length()-2].split(", ")

		obj_location[0] = (obj_location[0].to_f()/256.0 * 4).floor()/4.0
		obj_location[1] = (obj_location[1].to_f()/256.0 * 4).floor()/4.0

		obj_index[0] = obj_location[0]*4.0.floor()
		obj_index[1] = obj_location[1]*4.0.floor()

		if obj_location[0] < 0.0 || obj_location[0] >= $config_hash["core"]["map_size"][0] || obj_location[1] < 0.0 || obj_location[1] >= $config_hash["core"]["map_size"][1]
			store_delete_index << i
		elsif road_area[obj_index[0]][obj_index[1]] > 0
			store_delete_index << i
		end
	end


	for i in 0..store_delete_index.length()-1 do
		$base_hash['world']['levels']['0']['objects'].delete_at(store_delete_index[i]-i)
	end

end

def scoreroadwalk(maparray,road_walk)
	score = 0.0
	margin = ($config_hash["road"]["margin_percentage"]*$config_hash["core"]["map_size"][1]*4).floor()

	for i in 0..road_walk.length()-1 do
		if road_walk[i][0] < $config_hash["core"]["map_size"][0]*4 && road_walk[i][1] < $config_hash["core"]["map_size"][1]*4
			score = (score * i + maparray[road_walk[i][0]][road_walk[i][1]])/(i+1).to_f()
			if road_walk[i][1] <= margin || road_walk[i][1] >= $config_hash["core"]["map_size"][1]*4 - margin

				score = (score * i + 0.25 * maparray[road_walk[i][0]][road_walk[i][1]])/(i+1).to_f()
			else
				score = (score * i + maparray[road_walk[i][0]][road_walk[i][1]])/(i+1).to_f()
			end
			#puts "i: #{i}, score: #{score.floor(1)}, x,y: #{road_walk[i][0]/4.0}, #{road_walk[i][1]/4.0}"
		end
	end

	return score
end

def findbestwalk(maparray)

	walk_options = Array.new(){Array.new(){Array.new(2)}}
	start_options = Array.new(){Array.new(2)}
	margin = ($config_hash["road"]["margin_percentage"]*$config_hash["core"]["map_size"][1]*4).floor()+$config_hash["road"]["width"]*4
	best_score = 0.0
	bext_index = 0
	t = Time.now

	puts "Finding best walk..."
	for i in margin..$config_hash["core"]["map_size"][1]*4-margin-1 do
		start_options << [i,0]

		road_walk = goforawalk(maparray,[i,0],0,$config_hash["road"]["fov_angle"]/360*Math::PI*2,$config_hash["road"]["step_distance"])

		score = scoreroadwalk(maparray,road_walk)

		if road_walk[-1][0] < $config_hash["core"]["map_size"][0]*4
			score = 0.0
		end
		if score > best_score
			best_score = score
			best_index = i - margin
		end
		walk_options << road_walk
	end
	puts "Found best walk. Time taken: #{(Time.now-t).floor(3)}s."


	road_walk = walk_options[best_index]
	for i in 0..road_walk.length()-1 do
		road_walk[i][0] *= 0.25
		road_walk[i][1] *= 0.25
	end

	return road_walk

end


def makeperpendicularpath(sourcepath,distance,islefthand)

	location = Array.new(2)
	output_array = Array.new(){Array.new(2)}

	if islefthand
		angle = -Math::PI
	else
		angle = 0.0
	end

	for i in 0..sourcepath.length()-1 do

		if i == 0 || i == sourcepath.length()-1
			location = calculate_perpendicular_position(sourcepath[i][0],sourcepath[i][1], angle, distance)
			output_array << location

		elsif i > 0 && i < sourcepath.length()-1
			direction = Math.atan2(sourcepath[i+1][1]-sourcepath[i-1][1],sourcepath[i+1][0]-sourcepath[i-1][0]) + angle
			location = calculate_perpendicular_position(sourcepath[i][0],sourcepath[i][1], direction, distance)
			output_array << location

		end
	end
	return output_array

end

def make_side_river_flows(flowpath, flow_min_start)

	flow_length_variation = $config_hash["road"]["river"]["side_flowpath"]["flow_length_variation"].floor()
	last_flow_end = flow_min_start
	num_flows = (flowpath.length()*$config_hash["road"]["river"]["side_flowpath"]["flow_density"]/10.0).floor()

	if $config_hash["road"]["river"]["side_flowpath"]["reverse_direction"]
		flowpath = flowpath.reverse()
	end

	for i in 0..num_flows-1 do
		flow_length = (rand(-flow_length_variation..flow_length_variation)+$config_hash["road"]["river"]["side_flowpath"]["flow_length"]).floor()*2
		flow_start = last_flow_end + rand(1..$config_hash["road"]["river"]["side_flowpath"]["gap"].floor()) + (flowpath.length()/num_flows).floor() - flow_length
		if flow_start+flow_length < flowpath.length()-1
			smallflowpath = flowpath[flow_start..flow_start+flow_length]
			draw_path(smallflowpath, 2, $config_hash["road"]["river"]["side_flowpath"]["width"], $config_hash["road"]["river"]["side_flowpath"]["texture"],100,false)
		end
		last_flow_end = flow_start+flow_length
	end
end

def make_small_river_flows(sidepath, islefthand)

	flow_bezier_density = 1.0
	flow_length_variation = $config_hash["road"]["river"]["flowpath"]["flow_length_variation"]
	num_flow_variants = $config_hash["road"]["river"]["flowpath"]["num_flow_variants"]
	flowpaths = Array.new(num_flow_variants){Array.new(){Array.new(2)}}

	num_flows = (sidepath.length()*$config_hash["road"]["river"]["flowpath"]["flow_density"]/10.0).floor()

	distance_range = [$config_hash["road"]["river"]["flowpath"]["min_distance_from_edge"],$config_hash["road"]["width"]/2.0-$config_hash["road"]["river"]["flowpath"]["min_distance_from_middle"]]

	for i in 0..num_flow_variants-1 do

		delta = i*(distance_range[1]-distance_range[0])/(num_flow_variants-1) + distance_range[0]
		flowpath = makeperpendicularpath(sidepath,delta,islefthand)
		flowpaths[i] = makebeziercurve(flowpath,flow_bezier_density)

	end

	sectionlength = (sidepath.length()/num_flows).floor()

	for i in 0..num_flows-1 do

		flow_length = (rand(-flow_length_variation/2..flow_length_variation)+$config_hash["road"]["river"]["flowpath"]["flow_length"]).floor()*2
		section = i
		start_within_section = rand((sectionlength*0.1).floor()..(sectionlength*0.9).floor())
		flow_start = sectionlength*section+start_within_section
		if flow_start+flow_length < flowpaths[0].length()-1

			select_flow = ((1-Math.sqrt(rand()))*num_flow_variants).floor()
			smallflowpath = flowpaths[select_flow][flow_start..flow_start+flow_length]
			if !islefthand
				smallflowpath = smallflowpath.reverse()
			end
			if [true, false].sample && $config_hash["road"]["river"]["flowpath"]["randomdirection"]
				smallflowpath = smallflowpath.reverse()
			end
			draw_path(smallflowpath, 2, $config_hash["road"]["river"]["flowpath"]["width"], $config_hash["road"]["river"]["flowpath"]["texture"],100,false)
		end
	end



end

def make_river_flows(leftpath, rightpath)

	flowpath = Array.new(){Array.new(2)}
	smallflowpath = Array.new(){Array.new(2)}
	leftpath_points = Array.new(){Array.new(2)}
	flow_bezier_density = 1.0

	#Left hand bank
	flowpath = makeperpendicularpath(leftpath,$config_hash["road"]["river"]["edge_flowpath"]["offset"],true)
	if $config_hash["road"]["river"]["edge_flowpath"]["reverse_direction"]
		flowpath = flowpath.reverse()
	end
	draw_path(flowpath, 1, $config_hash["road"]["river"]["edge_flowpath"]["width"], $config_hash["road"]["river"]["edge_flowpath"]["texture"],100,false)

	#Right hand bank
	flowpath = makeperpendicularpath(rightpath,$config_hash["road"]["river"]["edge_flowpath"]["offset"],false)
	if $config_hash["road"]["river"]["edge_flowpath"]["reverse_direction"]
		flowpath = flowpath.reverse()
	end
	draw_path(flowpath.reverse(), 1, $config_hash["road"]["river"]["edge_flowpath"]["width"], $config_hash["road"]["river"]["edge_flowpath"]["texture"],100,false)

	leftpath_points = makebeziercurve(leftpath,flow_bezier_density)
	flowpath = makeperpendicularpath(leftpath_points,$config_hash["road"]["river"]["side_flowpath"]["offset"],true)
	make_side_river_flows(flowpath, 0)

	rightpath_points = makebeziercurve(rightpath,flow_bezier_density)
	flowpath = makeperpendicularpath(rightpath_points,$config_hash["road"]["river"]["side_flowpath"]["offset"],false)
	make_side_river_flows(flowpath.reverse(), 0)

	make_small_river_flows(leftpath_points,true)
	make_small_river_flows(rightpath_points,false)


end

def make_river_edges(leftpath, rightpath)

	under_edge_layer = 100
	edge_layer = 100

	if $config_hash["road"]["edge"].has_key?("under_edge_layer")
		under_edge_layer = $config_hash["road"]["edge"]["under_edge_layer"]
	end

	if $config_hash["road"]["edge"].has_key?("under_edge_layer")
		edge_layer = $config_hash["road"]["edge"]["edge_layer"]
	end

	# If there is a secondary path that we want to place under the main one
	if $config_hash["road"]["edge"].has_key?("under_edge_texture")
		offsetleftpath = Array.new(){Array.new(2)}
		offsetrightpath = Array.new(){Array.new(2)}

		#Left hand bank
		offsetleftpath = makeperpendicularpath(leftpath,$config_hash["road"]["edge"]["under_edge_offset"],true)

		#Right hand bank
		offsetrightpath = makeperpendicularpath(rightpath,$config_hash["road"]["edge"]["under_edge_offset"],false)

		if $config_hash["road"]["edge"]["reverse_under_edge_texture"] == false
			draw_path(offsetleftpath.reverse(), 1, $config_hash["road"]["edge"]["under_edge_texture_width"], $config_hash["road"]["edge"]["under_edge_texture"],under_edge_layer,false)
			draw_path(offsetrightpath, 1, $config_hash["road"]["edge"]["under_edge_texture_width"], $config_hash["road"]["edge"]["under_edge_texture"],under_edge_layer,false)
		else
			draw_path(offsetleftpath, 1, $config_hash["road"]["edge"]["under_edge_texture_width"], $config_hash["road"]["edge"]["under_edge_texture"],under_edge_layer,false)
			draw_path(offsetrightpath.reverse(), 1, $config_hash["road"]["edge"]["under_edge_texture_width"], $config_hash["road"]["edge"]["under_edge_texture"],under_edge_layer,false)
		end
	end

	if $config_hash["road"]["edge"]["reverse_edge_texture"] == false
		draw_path(leftpath.reverse(), 1, $config_hash["road"]["edge"]["edge_texture_width"], $config_hash["road"]["edge"]["edge_texture"],edge_layer,false)
		draw_path(rightpath, 1, $config_hash["road"]["edge"]["edge_texture_width"], $config_hash["road"]["edge"]["edge_texture"],edge_layer,false)
	else
		draw_path(leftpath, 1, $config_hash["road"]["edge"]["edge_texture_width"], $config_hash["road"]["edge"]["edge_texture"],edge_layer,false)
		draw_path(rightpath.reverse(), 1, $config_hash["road"]["edge"]["edge_texture_width"], $config_hash["road"]["edge"]["edge_texture"],edge_layer,false)
	end
end

def make_road(simple_perlin_map,terrain)

	start = [0,0]
  leftpath = Array.new(){Array.new(2)}
  rightpath = Array.new(){Array.new(2)}
	width = $config_hash["road"]["width"]
	t = Time.now
	waterpolygonarray = Array.new(){Array.new(2)}

	puts "Making road."
	road_walk = findbestwalk(simple_perlin_map)

	leftpath = makeperpendicularpath(road_walk,width/2.0,true)
	rightpath = makeperpendicularpath(road_walk,width/2.0,false)

  left_path = makebeziercurve(leftpath,$config_hash["road"]["bezier_density"])
  right_path = makebeziercurve(rightpath,$config_hash["road"]["bezier_density"])

	# Correct the coordinates from y,x to x,y
	leftpath = transposearrayofcoords(leftpath)
	rightpath = transposearrayofcoords(rightpath)

	road_area = findroadarea(left_path,right_path,terrain)
	apply_terrain_to_road(simple_perlin_map,road_area,terrain)
	removeobjectsfromroad(road_area)


	if $config_hash.has_key?("lighting")
		if $config_hash["lighting"].has_key?("road_light") && $config_hash["lighting"]["draw_lights"]
			apply_lighting_to_road(road_walk)
		end
	end

	puts "Finished making road. Time taken #{(Time.now-t).floor(2)}s.\n\n"

	if $config_hash["road"]["river"]["make_river"]
		#Add on extra points at the start in order to not have depth 0 on left hand edge
		waterpolygonarray << [right_path[0][0]-4, right_path[0][1]]
		waterpolygonarray << [left_path[0][0]-4, left_path[0][1]]
		# The _path arrays are too dense so remove some points aligned to a truncation factor.
		truncate_path_factor = 3.0
		for i in 0..(left_path.length()/truncate_path_factor).floor()-1
			waterpolygonarray << left_path[(i*truncate_path_factor).floor()]
		end
		waterpolygonarray << [left_path[-1][0]+2, left_path[-1][1]]
		right_path = right_path.reverse()
		for i in 0..(right_path.length()/truncate_path_factor).floor()-1
			waterpolygonarray << right_path[(i*truncate_path_factor).floor()]
		end

		outputwater(waterpolygonarray.flatten())

		if $config_hash["road"]["river"].has_key?('water_pattern')
			if $config_hash["road"]["river"]["water_pattern"]["use_water_pattern"]
				draw_pattern(waterpolygonarray.flatten(), $config_hash["road"]["river"]["water_pattern"]["layer"], $config_hash["road"]["river"]["water_pattern"]["texture"], $config_hash["road"]["river"]["water_pattern"]["colour"], $config_hash["road"]["river"]["water_pattern"]["rotation_deg"])
			end
		end

		if $config_hash["road"]["river"].has_key?("side_flowpath") && $config_hash["road"]["river"].has_key?("flowpath") && $config_hash["road"]["river"].has_key?("edge_flowpath")
			make_river_flows(leftpath,rightpath)
		end

		make_river_edges(leftpath,rightpath)

	end

end


def apply_lighting_to_road(road_walk)

	num_lights = ($config_hash["core"]["map_size"][0]*$config_hash["lighting"]["road_light"]["light_density"]).floor()

	detailed_road = makebeziercurve(road_walk,$config_hash["road"]["bezier_density"])
	margin = 0.1

	for i in 0..num_lights do

		index = ( (detailed_road.length()*(1 - 2*margin))/num_lights * i + margin * detailed_road.length() ).floor()

		draw_light(detailed_road[index][0],detailed_road[index][1],$config_hash["lighting"]["road_light"]["light_colour"],$config_hash["lighting"]["road_light"]["light_intensity"],$config_hash["lighting"]["road_light"]["light_radius"])

	end

end

def apply_lighting()

	if !$config_hash.has_key?("lighting")
		return
	end
	if !$config_hash["lighting"]["draw_lights"]
		return
	end

	puts "Applying lighting effects."

	$base_hash['world']['levels']['0']['environment']['ambient_light'] = $config_hash["lighting"]["ambient_light"]

	viewport = [0,$config_hash["core"]["map_size"][0],0,$config_hash["core"]["map_size"][1]]
	light_min_distance = 1/$config_hash["lighting"]["primary_light"]["light_density"]

	poissonDisk = PoissonDisk.new(viewport, light_min_distance, $config_hash["poissondisk"]["max_tries"])
	points = poissonDisk.all()

	for i in 0..points.length()-1 do

		x = points[i][:x]
		y = points[i][:y]

		distance_from_middle = Math.sqrt((x-$config_hash["core"]["map_size"][0]/2.0)**2 + (y-$config_hash["core"]["map_size"][1]/2.0)**2)

		if distance_from_middle < $config_hash["core"]["map_size"][0]*(1-$config_hash["lighting"]["light_margin_percentage"])*0.5

			draw_light(x,y,$config_hash["lighting"]["primary_light"]["light_colour"],$config_hash["lighting"]["primary_light"]["light_intensity"],$config_hash["lighting"]["primary_light"]["light_radius"])
		end

	end

end

# START OF MAIN CODE

config_file_str = "config.json"
output_file_str = "undefined"
use_perlin_store = false
no_river_opts = false
override_mapsize = 0
override_perlinsize = 0
no_lights = false

# Parse options
parser = OptionParser.new do |opts|
	opts.banner = "Usage: randomforest.rb [options]"

  opts.on('-c config_file', 'Specify config file to use') do |config_file|
		config_file_str = config_file;
  end

	opts.on('-s', 'Forces the use of a stored perlin file if it exists even if the config file does not') do
		use_perlin_store = true;
	end

	opts.on('-o output_file', 'Specifies the output file name including the .dungeondraft_map extension') do |output_file|
		output_file_str = output_file;
  end

	opts.on('-d', 'Forces the code to only create a road not a river even if the config file does not specify this') do
		no_river_opts = true;
	end

	opts.on('-m map_size', 'Forces the code to overwrite the map size in the config file with this value') do |mapsize|
		override_mapsize = mapsize.to_i();
	end

	opts.on('-p perlin_size', 'Forces the code to overwrite the perlin size in the config file with this value') do |perlin_size|
		override_perlinsize = perlin_size.to_i();
	end

	opts.on('-l', 'Forces the code to ignore lighting config in the defined config file') do
		no_lights = true;
	end

	opts.on('-h', '--help', 'Displays Help') do
		puts opts
		exit
	end
end
parser.parse!
$config_hash = JSON.parse(File.read(config_file_str))

if use_perlin_store then
	$config_hash["perlin_noise"]["use_store"] = true
end
if output_file_str != "undefined"
	$config_hash["core"]["output_file"] = output_file_str
end
if no_river_opts then
	$config_hash["road"]["river"]["make_river"] = false
end
if override_mapsize > 0 then
	$config_hash["core"]["map_size"][0] = override_mapsize
	$config_hash["core"]["map_size"][1] = override_mapsize
end
if override_perlinsize > 0 then
	$config_hash["perlin_noise"]["size"] = override_perlinsize
end
if no_lights then
	$config_hash["lighting"]["draw_lights"] = false
end

puts "\n"
puts "Running Random Forest Generator for Dungeondraft."
puts "Config file used: #{config_file_str}"
puts "Config name: #{$config_hash["header"]["config_name"]}"
puts "Output Filename: #{$config_hash["core"]["output_file"]}"

if $config_hash["perlin_noise"]["use_store"]
	puts "Using Perlin Data Stored in: #{$config_hash["perlin_noise"]["store_file"]}"
end

if $config_hash["core"]["map_size"][0] != $config_hash["core"]["map_size"][1]
	puts "Defined map size, #{$config_hash["core"]["map_size"][0]} x #{$config_hash["core"]["map_size"][1]}, is not square. Exiting."
end

puts "Generating Map of Size: #{$config_hash["core"]["map_size"][0]} x #{$config_hash["core"]["map_size"][1]}"
puts

$base_hash = JSON.parse(File.read($config_hash["core"]["baseline_map_file"]))
$next_node_id = 0
update_core_map_hash()

if !$config_hash["perlin_noise"]["use_store"] then
  pixels = createperlin($config_hash["perlin_noise"]["size"],$config_hash["perlin_noise"]["granularity"],$config_hash["perlin_noise"]["octaves"])
  store_perlin(pixels, $config_hash["perlin_noise"]["store_file"])
else
  pixels = retrieve_perlin($config_hash["perlin_noise"]["store_file"])
	$config_hash["perlin_noise"]["size"] = pixels.length()
end

scatter_trees(pixels)
scatter_clumps(pixels)
scatter_objects(pixels)
apply_lighting()

if $config_hash["terrain"]["make_terrain"]

	if $base_hash['world']['levels']['0']['terrain'].has_key?('splat2')
		xtraterrain = true
		terrain = make_terrain_array($base_hash['world']['levels']['0']['terrain']['splat'],$base_hash['world']['levels']['0']['terrain']['splat2'],$config_hash["core"]["map_size"][0],$config_hash["core"]["map_size"][1])
	else
		terrain = make_terrain_array($base_hash['world']['levels']['0']['terrain']['splat'],"",$config_hash["core"]["map_size"][0],$config_hash["core"]["map_size"][1])
	end

	for i in 0..3 do
		if $config_hash["terrain"]["slots"][i]["overwrite_slot"]
			$base_hash['world']['levels']['0']['terrain']['texture_'+(i+1).to_s()] = $config_hash["terrain"]["slots"][i]["texture"]
		end
	end
	apply_perlin_to_terrain(pixels, terrain, $config_hash["perlin_noise"]["granularity"], $config_hash["perlin_noise"]["size"], $config_hash["core"]["map_size"][0],$config_hash["core"]["map_size"][1])

	if $config_hash["terrain"].has_key?("smooth_blending")
		if $config_hash["terrain"]["smooth_blending"]
			$base_hash['world']['levels']['0']['terrain'][:smooth_blending] = true
		end
	end
end

simple_perlin_map = simplifyperlin(pixels)

if $config_hash["perlin_noise"]["output_perlin_ppm"]
	outputppm($config_hash["perlin_noise"]["output_perlin_ppm_filename"],simple_perlin_map,$config_hash["core"]["map_size"][0]*4,$config_hash["core"]["map_size"][1]*4,$config_hash["perlin_noise"]["granularity"])
end

make_road(simple_perlin_map,terrain)

apply_shadows_to_trees()

output_terrain(terrain,false,$config_hash["core"]["map_size"][0],$config_hash["core"]["map_size"][1])

$base_hash['world']['next_node_id'] = $next_node_id.to_s(16)
File.open($config_hash["core"]["output_file"], "wb") { |file| file.puts JSON.pretty_generate($base_hash) }
puts "Dungeondraft file created: #{$config_hash["core"]["output_file"]}."
