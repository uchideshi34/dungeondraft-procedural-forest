# Dungeondraft Procedural Forest

## Introduction & Purpose

The purpose of this script is to reduce the time it takes to make a simple forest map using Dungeondraft which can then be further edited. You aren't going to get a great map from this script but you should get something usable.

I developed this more just to see if I could as the idea of procedural map creation interests me. I am not a developer (as anyone examining my work will clearly see) but have some background in script and algorithm development a while ago. There is limited error checking but it should work for the most part.

The script is written in Ruby and intended to be run from a command line. There are no fancy UI elements as that is well beyond me - someone may convert the logic into a Dungeondraft mod which will more user friendly.

Note that this script relies on a lot of configuration held in a config file. Changing almost all of the configuration is possible but requires a reasonable amount of effort and some knowledge of Dungeondraft map files.

## Ruby Language

Ruby is included natively on MacOS and Linux but it needs to be downloaded I understand onto Windows devices. It can be found here: https://rubyinstaller.org/downloads/.

## Script Scope

The script supports the following features:

* Creation of terrain using 4 different terrain types
* Creation of a tree layer including options for shadows as paths and objects (including directional shadows)
* Creation of clumps of assets such as a central object with multiple under them
* Creation of individual objects distributed across the map
* Support for random object rotation and mirroring as well as fixed values
* Creation of a road/river goes from the left hand edge to the right hand edge
* Blending of the road/river terrain
* Rivers: creation of a DD water layer as well as water pattern and multiple paths representing water flows
* Rivers: creation of up to two layered paths representing the banks of the river
* Lighting effects including placing lights to highlight the road/river and the tops of trees
* Variable square map sizes as supported by DD
* Storage and retrieval of previous Perlin maps

**Out of scope**

* Up to 8 terrain types
* Rivers: rocks within the river and flows for them
* Collision/Overlap mitigation for different clumps or objects
* Rectangular map sizes

## User Guide

### Basic Operation

1. Download the files "randomforest v1.0.rb", "baseline.dungeondraft_map" and "config.json" and put them into a suitable directory.
2. As required make the "randomforest v1.0.rb" as executable, e.g. "chmod u+x randomforest v1.0.rb"
2. For shadows features, download [Kragers asset pack](https://cartographyassets.com/assets/7713/kragers-shadow-light-pack/) and put it in your Dungeondraft asset folder.
3. Other example files (baseline and config files) can be found for Dungeon Mapster, Forgotten Assets and Crosshead assets. These represent a reasonable starting point using those assets providers but can be modified as needed.
4. Run the following command from a terminal window, replacing with <configfile.json> with the name of your config file, e.g. "config.json":

```
ruby randomforest\ v1.0.rb -c <configfile.json>
```

Further options for running the script can be found by typing: 

```
ruby randomforest\ v1.0.rb -h
```

### Modifying and Using the Config File

The config file, e.g. "config-DM.json", is a simple JSON file that contains the various configuration parameters for conversion. Most parameters are available as a customised option but they often require knowing the exact texture path of the asset in question which you would need to extract from an example dungeondraft_map file opened in a text editor.

There are a large number of options for configuring the business logic behind the script or which assets are used but require some knowledge how the script works or exact texture path of the asset in Dungeondraft.

The config file itself is called as a parameter when running the script so you can have multiple config files for different map themes.

```
ruby randomforest\ v1.0.rb -c <configfile.json>
```

Detailed instructions for modifying the config file and parameters contained within can be found below.

### Using 3rd Party Assets

A lot of people, myself included, prefer to use 3rd party assets in Dungeondraft rather than the in built assets. The process for doing this is relatively straightforward:

* Open the "baseline.dungeondraft_map" in Dungeondraft
* Add the asset packs that you want to use to that map file and save
* Optionally, if you save it as a different file name, you need to update the config file accordingly
* Extract the texture names as needed, noting that for walls and patterns it may be quicker just to click and replace in DD once the new map file is made.

I have prebuilt some examples using Dungeon Mapster, Crosshead and Forgotten Adventures assets.

### Detailed Configuration File Guidance

The config file is a simple JSON format and you can edit it in any text editor. I would always recommend checking it with the JSON validator once done as it is easy to make typos.

A number of config file entries are direct references to asset textures. To find these references to include in the config file, the easiest way is to open any blank DD file and add any relevant 3rd party assets packs. Then add the asset that you are looking for, e.g. a door portal asset, and save the file. Open the DD map file in a text editor and search for that reference, e.g. "portals" search will take you to the JSON section containing portals and you will see an entry for the door portal that you added.

 

**Header Section**

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| config_name      | String       | Yes   | A name for the config file that will be displayed when the config is used | My Favourite Config File |
| version   | String        | No      | A version identifier - this is not used in the script and is simply for your own needs. | v1.0 |
| asset_style   | String        | No      | A description of the 3rd party assets used - this is not used in the script and is simply for your own needs. | 2MTT |

**Core Section**

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| map_size      | Array of Integers       | Yes   | Size in squares of the map to be produced | [32,32] |
| baseline_map_file   | String        | Yes      | The filename of the baseline map file | baseline.dungeondraft_map |
| output_file   | String        | Yes      | The filename of the output map file. | test_output.dungeondraft_map |

**Perlin Noise Section**

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| permutation_size      | Integer       | Yes   | I'd leave this alone  | 256 |
| size   | Integer        | Yes      | The number of pixels in each row & column of the base Perlin Noise array. 768 works well for smaller maps. | 1024 |
| granularity   | Integer        | Yes      | Translates the perlin noise between 0.0 and 1.0 into an integer value | 16 |
| octaves   | Integer        | Yes      | Number of fractal octaves. I'd leave this alone. | 4 |
| store_file   | Text        | Yes      | The filename where the perlin map will be stored | perlin_store.txt |
| use_store   | Boolean        | Yes      | Instructs the script to use a stored perlin file rather than create a new one | false |
| output_perlin_ppm   | Boolean        | Yes      | Used for debugging it outputs a graphical respresentation of the perlin file if true. | false |
| output_perlin_ppm_filename   | String        | Yes      | Used for debugging it outputs a graphical respresentation of the perlin file | simpleperlin.ppm |


**Poisson Disc Section**

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| max_tries      | Integer       | Yes   | It is a parameter of the poisson disc algorithm. I'd leave this alone  | 30 |

**Lighting Section**

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| draw_lights      | Boolean       | Yes   | Required in order to draw lights.  | true |
| ambient_light      | String       | Yes   | The hex colour code of the ambient light value to be used   | ff646464 |
| light_margin_percentage   | Float        | Yes      | The fraction of the map that no primary lights will be placed in, e.g. 0.1 is 10% | 0.1 |
| primary_light   | JSON        | Yes      | A JSON section defining the config parameters of the main lights. | n/a |
| road_light   | JSON        | Yes      | A JSON section defining the config parameters of the lights along the road/river. | n/a |

*primary_light*

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| light_colour      | String       | Yes   | The hex colour code of the light value to be used   | ff646464 |
| light_intensity   | Float        | Yes      | The intensity of the light, in general you want this to be 0.5 | 0.5 |
| light_radius   | Float        | Yes      | The radius of the light created - I'd suggest keeping this at 10 or greater. | 15 |
| light_density   | Float        | Yes      | Defines the density of the lights - the minimum distance between lights is the inverse of this value. | 0.21 |

*road_light*

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| light_colour      | String       | Yes   | The hex colour code of the light value to be used   | ff646464 |
| light_intensity   | Float        | Yes      | The intensity of the light, in general you want this to be 0.5 | 0.5 |
| light_radius   | Float        | Yes      | The radius of the light created - I'd suggest keeping this at 5 or greater. | 5 |
| light_density   | Float        | Yes      | Defines the density of the lights - the number of lights along the road is this value multiplied by the width of the map. | 0.2 |

**Terrain Section**

Terrain is created by using the perlin noise map and applying the following business logic.

[![Preview Image](Images/terrain_logic.webp)](https://gitlab.com/gull-rock-maps/dungeondraft-procedural-forest/-/blob/main/Images/terrain_logic.webp)

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| make_terrain      | Boolean       | Yes   | Always true  | true |
| smooth_blending   | Boolean        | No      | If true then smooth blending option of terrain is enabled. In general, not using smooth blending is better in my opinion. | false |
| high_level   | Integer        | Yes      | Must be between 0 and 255. Used as per table above.  | 180 |
| low_level   | Integer        | Yes      | Must be between 0 and 255. Used as per table above.  | 50 |
| flex   | Integer        | Yes      | Must be between 0 and 255. Used as per table above.  | 50 |
| open_area_weight   | Float        | Yes      | Used as per table above.  | 1.25 |
| highest_weight   | Float        | Yes      | Used as per table above.  | 0.4 |
| low_weight   | Float        | Yes      | Used as per table above.  | 0.5 |
| low_flex_factor   | Float        | Yes      | Used as per table above.  | 0.75 |
| high_flex_factor   | Float        | Yes      | Used as per table above.  | 0.5 |
| slots   | Array        | Yes      | A list of the four terrain JSON entries for each slot.  | n/a |

*slots*
| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| name      | String       | Yes   | A description of what the slot represents. This has no active part in the code.  | "Highest under trees" |
| id   | Integer        | No      | An id tag for the slot. This is not actually used in the code. | 0 |
| overwrite_slot   | Boolean        | Yes      | This ensures that the terrain slot in the baseline map file is overwritten and should always be set to true. | true |
| colour   | Array        | No      | This was a rgb array used for debugging and is not used in the execution code. | [120,120,255] |
| texture   | String        | No      | This is the url for the texture of the asset for terrain. | ... |

**Road Section**

The road (or river) is created by starting on the left of the map and then looking ahead in a field of view to all possible next location points at a defined distance and choosing one with the highest perlin value. The choice of next location is weighted to prefer movements towards the right hand edge and avoid locations close to the top and bottom areas of the map. Multiple start locations on the left hand edge tried and the path with the best score (highest average perlin value) is selected as the final route for the road.

Any trees or other objects are removed from the road area. If defined, additional lights are placed along the road to highlight its visual importance.

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| step_distance      | Float       | Yes   | The number of squares ahead that the alogrithm looks for the next step location. The lower the value the more the road tends to wobble. | 2 |
| progress_weight   | Float        | Yes      | A weighting factor to try and ensure the road goes from left to right. The higher the value the straighter the road will be. Circular roads or failed roads may occur if the value is too low. | 0.25 |
| fov_angle   | Float        | Yes      | The total field is twice this value in degrees. A narrower FOV results in straighter roads, higher values increase the likelihood of circular or failed roads. | 60.0 |
| margin_percentage   | Float        | Yes      | This is the fraction of the top and bottom of the map that you want the road to avoid. | 0.2 |
| width   | Float        | Yes      | This is the width of the road in squares. | 5 |
| bezier_density   | Float        | Yes      | This is used to convert the road path into location values for the road area. Leave this alone. | 0.1 |
| terrain   | JSON        | No      | This is a JSON structure that defines how terrain values in the road area are varied. More details below. | n/a |
| edge   | JSON        | Yes      | This is a JSON structure that defines the edges of the road particularly for rivers. More details below. | n/a |
| river   | JSON        | No      | This is a JSON structure that defines the parameters for making a river. More details below. | n/a |

*terrain*

If enabled, the script will vary the balance of the road terrain between the first terrain slot (usually a dirt texture) and fourth terrain slot (either another dirt or rocky texture) to make it look a bit more interesting.

The logic creates a base terrain map for the road of those two textures based on the perlin noise map and distance from the edge of the road. The final terrain then takes that base and blends it with the underlying "road-less" terrain based on the distance from the edge of the road.

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| vary_road_terrain      | Boolean       | Yes   | A boolean option to turn off the terrain variation even if the config is present. Mostly used for debugging. | true |
| edge_terrain_1_value_255   | Integer        | Yes      | Defines the value of the terrain 1 at the edge of the road. | 200 |
| terrain_1_lowest_value   | Integer        | Yes      | The lowest level that terrain 1 can be. The lower the value the more terrain 4 will dominate the resulting road terrain. | 50 |
| terrain_1_highest_value   | Integer        | Yes      | The highest level that terrain 1 can be. The lower the value the more terrain 4 will dominate the resulting road terrain. Note the higher the difference between the high and low values the more varied the terrain blend will be along the length of the road. | 150 |

*edge*

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| roadedge_width      | Float       | Yes   | The width of the road edge in squares this is used to blend the surrounding terrain into the road terrain. | 2 |
| edge_terrain_value   | Integer        | No      | This was used for debugging and is not required. | 90 |
| edge_texture   | String        | No      | If making a river, this is texture for the path to be used for the higher river edge. | ... |
| edge_texture_width   | Integer        | No      | If making a river, this is width of the path in pixels to be used for the higher river edge. | 150 |
| reverse_edge_texture   | Boolean        | No      | If making a river, this defines whether you want to reverse the direction of the path for the higher river edge. | true |
| edge_texture_layer   | Integer        | No      | If making a river, this is layer of the path to be used for the higher river edge. | 100 |
| under_edge_texture   | String        | No      | If making a river, this is texture for the path to be used for the lower river edge. | ... |
| under_edge_texture_width   | Integer        | No      | If making a river, this is width of the path in pixels to be used for the lower river edge. | 150 |
| under_reverse_edge_texture   | Boolean        | No      | If making a river, this defines whether you want to reverse the direction of the path for the lower river edge. | true |
| under_edge_texture_layer   | Integer        | No      | If making a river, this is layer of the path to be used for the higher river edge. | 100 |

*river*

A river is a road with the following additional features:
*One or two paths that line the edge of the road
*Water drawn with the DD water tool over the area of the road
*Optionally a pattern laid over the water area usually a semitransparent texture
*Edge "water flow" paths all the way along each river edge
*A series of side "water flow" paths a small distance off the river edge.
*A series of other smaller "water flow" paths randomly distributed along the length of the river and across its width


| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| make_river      | Boolean       | Yes   | A switch to enable or disable making a river even if the config is available. | true |
| deep_color   | String        | Yes      | The deep colour for use in the water tool. | "ff4d9cc7" |
| shallow_color   | String        | Yes      | The shallow colour for use in the water tool. | "ff4d9cc7" |
| blend_distance   | Float        | Yes      | The blend distance used in the water tool. | 2.0 |
| water_pattern   | JSON        | No      | Defines the parameters for an overlaid water pattern. | n/a |
| edge_flowpath   | JSON        | No      | Defines the parameters for the edge water paths. | n/a |
| side_flowpath   | JSON        | No      | Defines the parameters for the side water paths. | n/a |
| flowpath   | JSON        | No      | Defines the parameters for the other water paths. | n/a |

*water_pattern*

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| use_water_pattern      | Boolean       | Yes   | A switch to enable or disable making a river even if the config is available. | true |
| layer   | Integer        | Yes      | The layer that the water pattern will put on. | 100 |
| rotation_deg   | Float        | Yes      | Rotation angle in degrees of the pattern texture. | 0.0 |
| texture   | String        | Yes      | The texture of the pattern to be used. | ... |
| colour   | String        | Yes      | The colour of the water texture. | "ff4d9cc7" |

*edge_flowpath*

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| texture   | String        | Yes      | The texture of the water path to be used. | ... |
| width   | Integer        | Yes      | The width of the water path to be used. | 256 |
| offset   | Float        | Yes      | The offset from the true edge of the road where the path will be drawn. | 0.2 |
| reverse_direction      | Boolean       | No   | An option to reverse the direction of the path. | false |


*side_flowpath*

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| texture   | String        | Yes      | The texture of the water path to be used. | ... |
| width   | Integer        | Yes      | The width of the water path to be used. | 256 |
| offset   | Float        | Yes      | The offset from the true edge of the road where the path will be drawn. | 0.2 |
| reverse_direction      | Boolean       | No   | An option to reverse the direction of the path. | false |
| randomdirection      | Boolean       | No   | An option to randomise the direction of the path. | false |
| flow_density      | Float       | Yes   | Defines how many flows will be made linearly scaling with the length of the river. | 0.2 |
| end_type      | String       | Yes   | Defines the end type of the path. Don't change. | "Shrink" |
| flow_length      | Float       | Yes   | Average length of the side flow in squares. | 8 |
| flow_length_variation      | Float       | Yes   | The varation possible in the side flow length in squares. | 1 |
| gap      | Float       | Yes   | The maximum gap between the last flow and the next flow. | 1 |

*flowpath*

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| texture   | String        | Yes      | The texture of the water path to be used. | ... |
| width   | Integer        | Yes      | The width of the water path to be used. | 256 |
| reverse_direction      | Boolean       | No   | An option to reverse the direction of the path. | false |
| randomdirection      | Boolean       | No   | An option to randomise the direction of the path. | false |
| flow_density      | Float       | Yes   | Defines how many flows will be made linearly scaling with the length of the river. | 0.2 |
| flow_length      | Float       | Yes   | Average length of the side flow in squares. | 8 |
| flow_length_variation      | Float       | Yes   | The varation possible in the side flow length in squares. | 1 |
| num_flow_variants      | Integer       | Yes   | The number of distance options from the edge to the centre of the river for the flows to be drawn. | 5 |
| min_distance_from_middle      | Float       | Yes   | The minimum distance in squares from the middle of the river that the flows could be drawn. | 0.25 |
| min_distance_from_edge      | Float       | Yes   | The minimum distance in squares from the edge of the river that the flows could be drawn. | 1.25 |

**Objects Section (Trees, Clutter and Clumps)**

The distribution of objects on the map follow the same basic logic with some additional features for trees and clumps.

The script uses a Poisson Disc algorithm to define a set of potential object locations across the map area with a minimum distance between those locations. It then examines the perlin noise value at that location and if the value is in a particular range that location is valid for that object set. For each valid location, the script then applies a uniform probability to determine whether an object should actually be placed at that location.

**Trees Section**

A tree set is a standard object set with the optional features for placing shadows and/or to place a light asset.
The tree JSON structure use tree root with tree_type_list as an array of JSON tree objects as below.
For the examples I defined two tree types ("big trees" and "small trees") with non-overlapping perlin noise ranges to ensure they don't occupy exactly the same space. I think this is probably the best way to manage trees and stop them overlapping each other too much.

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| mindistance   | Float        | Yes      | The minimum distance between trees in squares - this is used in the possion disc algorithm. | 2.2 |
| probability   | Float        | Yes      | The probability of placing a tree on a valid location. | 1.0 |
| upper_perlin_level      | Float       | Yes   | The upper value of the perlin noise that defines a valid location. | 0.45 |
| lower_perlin_level      | Float       | Yes   |  The lower value of the perlin noise that defines a valid location. | 0.0 |
| randomrotation      | Boolean       | No   | An option to randomise the rotation of the object. | false |
| randommirror      | Boolean       | No   | An option to randomise the mirror status of the object. | false |
| size      | Array of Floats       | Yes   | A two element array defining the lower and upper size multipliers for object size | [1.0,1.1] |
| layer      | Integer     | Yes   | The layer that the tree will be placed on | 400 |
| list      | Array of Strings     | Yes   | An array of texture strings that will be selected at random to determine which is used for a placed tree | ... |
| shadows      | JSON     | No   | A JSON structure for the parameters for adding shadows if required | n/a |
| lights      | JSON     | No   | A JSON structure for the parameters for adding lights if required | n/a |
| roation_offset   | Float        | No      | If defined, the tree is always rotated this value in degrees. Needed for tree assets with implied sun direction. | 100.0 |


*shadows*

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| make_shadow      | Boolean       | Yes   | A switch to enable or disable making a shadows even if the config is available. | true |
| type   | String        | Yes      | The type of shadow to be drawn either "path" or "object". | "path" |
| size   | Float        | Yes      | The width of the path in pixels if a path or the size multiplier if an object. | 500 |
| asset_radius   | Float        | No      | If a path, then this is the radius of the path to be drawn around the centre point. | 400 |
| texture   | String        | No      | If a path, then this is the texture  of the path to be drawn. | ... |
| texture_list   | Array or Strings        | No      | If an object, then this is the list of textures of the object to be placed. | ... |
| randommirror      | Boolean       | No   | If an object, allows random mirroring of the shadow object. | true |
| place_under      | Boolean       | No   | If an object, defines whether the shadow object is placed above or below the tree. | true |
| roation_offset   | Float        | No      | If defined for an object, the shadow is always rotated this value in degrees relateive to the tree offset. Needed for tree assets with implied sun direction. | 100.0 |
| centre_offset_dist   | Float        | No      | If defined for an object, the shadow is placed this value in distance relative to the tree. | 100.0 |
| centre_offset_angle   | Float        | No      | If defined for an object, the shadow is placed at this angle relative to the tree. | 0.0 |

*lights*

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| draw_lights      | Boolean       | Yes   | A switch to enable or disable making a shadows even if the config is available. | true |
| light_colour      | String       | Yes   | The hex colour code of the light value to be used   | ff646464 |
| light_intensity   | Float        | Yes      | The intensity of the light. | 0.5 |
| light_radius   | Float        | Yes      | The radius of the light created - I'd suggest keeping this around 1. | 1 |


**Clumps Section**

A clump set is a standard object set with the extra feature to place series of other objects nearby. This is useful for creating clumps of individual plants or for placing a large rock and then pebbles underneath it. Making clumps is pretty standard in mapmaking for forests and aids the general natural feeling for a map.

A clump set is an array of JSON structures.

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| mindistance   | Float        | Yes      | The minimum distance between clumps in squares - this is used in the possion disc algorithm. | 2.2 |
| probability   | Float        | Yes      | The probability of placing a clump on a valid location. | 1.0 |
| upper_perlin_level      | Float       | Yes   | The upper value of the perlin noise that defines a valid location. | 0.45 |
| lower_perlin_level      | Float       | Yes   |  The lower value of the perlin noise that defines a valid location. | 0.0 |
| size      | Array of Floats       | Yes   | A two element array defining the lower and upper size multipliers for object size | [1.0,1.1] |
| layer      | Integer     | Yes   | The layer that the primary asset of the clump will be placed on | 200 |
| primary_asset_list      | Array of JSON     | Yes   | An array of JSON structures randomly chosed each of which defines a set of parameters for that clump | ... |
| secondary_randomrotation      | Boolean       | No   | An option to randomise the rotation of the secondary object. | false |
| secondary_randommirror      | Boolean       | No   | An option to randomise the mirror status of the secondary object. | false |
| secondary_layer      | Integer     | Yes   | The layer that the secondary asset of the clump will be placed on | 100 |
| secondary_asset_list   | Array or Strings        | Yes      | This is the list of textures of the secondary objects to be randomly selected and placed. | ... |

*primary_asset_list*

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| texture   | String        | Yes      | The texture of the primary asset to be placed. | ... |
| num_secondary_assets_range   | Array of Integers        | Yes      | A two element array defining the lower and upper number of secondary assets to be placed. | 1.0 |
| secondary_assets_dist_px      | Float       | Yes   | The median pixel distance that a secodnary asset will be placed from the primary one. | 100 |
| secondary_assets_dist_variation_px      | Float       | Yes   |  The variation in the distance for asset distance. | 25 |
| randomrotation      | Boolean       | No   | An option to randomise the rotation of the primary object. | false |
| randommirror      | Boolean       | No   | An option to randomise the mirror status of the primary object. | false |


**Clutter Section**

Clutter is basically a standard object set. Only single objects are placed. Useful for medium sized plant patches or collections of rocks.

| ID      | Format | Required | Description | Example      |
| :---        |    :--- |    :--- |    :---   |         :--- |
| mindistance   | Float        | Yes      | The minimum distance between objects in squares - this is used in the possion disc algorithm. | 2.2 |
| probability   | Float        | Yes      | The probability of placing an object on a valid location. | 1.0 |
| upper_perlin_level      | Float       | Yes   | The upper value of the perlin noise that defines a valid location. | 0.45 |
| lower_perlin_level      | Float       | Yes   |  The lower value of the perlin noise that defines a valid location. | 0.0 |
| randomrotation      | Boolean       | No   | An option to randomise the rotation of the object. | false |
| randommirror      | Boolean       | No   | An option to randomise the mirror status of the object. | false |
| size      | Array of Floats       | Yes   | A two element array defining the lower and upper size multipliers for object size | [1.0,1.1] |
| layer      | Integer     | Yes   | The layer that the object will be placed on | 400 |
| list      | Array of Strings     | Yes   | An array of texture strings that will be selected at random to determine which is used for a placed object | ... |
